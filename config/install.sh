#!/bin/bash
set -euox pipefail

echo ["$(date "+%H:%M:%S")"] "==> Installing packages…"

apk update && apk add --no-cache git unzip curl libstdc++

echo ["$(date "+%H:%M:%S")"] "==> Installing asdf…"
mkdir -p "$ASDF_DATA_DIR"
git clone https://github.com/asdf-vm/asdf.git "$ASDF_DATA_DIR" --branch "$ASDF_VERSION"

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/asdf.sh

# This fork of asdf-ant is necessary for:
# 1. supporting arm64 architecture.
# 2. providing set-ant-home scripts for bash and zsh.
asdf plugin add ant https://gitlab.com/theoretick/asdf-ant

asdf plugin add java
asdf plugin add gradle
asdf plugin add grails
asdf plugin add maven
asdf plugin add sbt
asdf plugin add scala

asdf install
asdf reshim
asdf current

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/plugins/java/set-java-home.bash
# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/plugins/ant/set-ant-home.bash

# Ensure that Java is working correctly by printing the version.
asdf list java | while read -r jdk_version ; do
  java_bin="$(asdf which java "$jdk_version")"
  $java_bin -version 2&> /dev/null
  exit_code=$?
  if [ $exit_code -ne 0 ]; then
      echo "Java installation check failed. Attempted to run '$java_bin -version' and got exit code $exit_code" && exit 1
  fi
done;

# Install SpotBugs CLI
cd /spotbugs; \
  mkdir -p dist; \
  curl -LO "https://repo.maven.apache.org/maven2/com/github/spotbugs/spotbugs/${SCANNER_VERSION}/spotbugs-${SCANNER_VERSION}.tgz"; \
  tar xzf "spotbugs-${SCANNER_VERSION}.tgz" -C dist --strip-components 1; \
  rm -f "spotbugs-${SCANNER_VERSION}.tgz"

# Install FindSecBugs for use as a SpotBugs plugin
mkdir -p /fsb; \
  cd /fsb; \
  curl -LO "https://github.com/find-sec-bugs/find-sec-bugs/releases/download/version-${FINDSECBUGS_VERSION}/findsecbugs-cli-${FINDSECBUGS_VERSION}.zip"; \
  unzip -n "findsecbugs-cli-${FINDSECBUGS_VERSION}.zip"; \
  rm -f "findsecbugs-cli-${FINDSECBUGS_VERSION}.zip"; \
  mv "lib/findsecbugs-plugin-${FINDSECBUGS_VERSION}.jar" lib/findsecbugs-plugin.jar

echo ["$(date "+%H:%M:%S")"] "==> Beginning cleanup…"

rm -fr \
  "$ASDF_DATA_DIR/docs" \
  "$ASDF_DATA_DIR"/installs/java/**/demo \
  "$ASDF_DATA_DIR"/installs/java/**/man \
  "$ASDF_DATA_DIR"/installs/java/**/sample \
  "$ASDF_DATA_DIR"/installs/grails/**/media \
  "$ASDF_DATA_DIR"/installs/scala/**/doc \
  "$ASDF_DATA_DIR"/installs/scala/**/man \
  "$ASDF_DATA_DIR"/installs/**/**/share \
  "$ASDF_DATA_DIR"/test

echo ["$(date "+%H:%M:%S")"] "==> Done"
