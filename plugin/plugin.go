package plugin

import (
	"os"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/plugin"

	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v5/project"
)

// Match checks if this project can be built by one of our supported builders:
func Match(_ string, info os.FileInfo) (bool, error) {
	return project.HasBuilder(info), nil
}

func init() {
	plugin.Register("spotbugs", Match)
}
