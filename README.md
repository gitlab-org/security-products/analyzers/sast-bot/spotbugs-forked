# spotbugs analyzer

spotbugs analyzer performs SAST scanning on repositories containing code written in the following language: `java`.

The analyzer wraps [SpotBugs](https://spotbugs.github.io/) and [find-sec-bugs](https://find-sec-bugs.github.io/), and is written in Go. It's structured similarly to other Static Analysis analyzers because it uses the shared [command](https://gitlab.com/gitlab-org/security-products/analyzers/command) package.

The analyzer is built and published as a Docker image in the GitLab Container Registry associated with this repository. You would typically use this analyzer in the context of a [SAST](https://docs.gitlab.com/ee/user/application_security/sast) job in your CI/CD pipeline. However, if you're contributing to the analyzer or you need to debug a problem, you can run, debug, and test locally using Docker.

For instructions on local development, please refer to the [README in Analyzer Scripts](https://gitlab.com/gitlab-org/secure/tools/analyzer-scripts/-/blob/master/analyzers-common-readme.md).

## How are the JDK installed and configured?

To run scans inside the container built on the Docker image mentioned above, the analyzer uses [asdf](https://asdf-vm.com) to install JDK and other dependencies (e.g. ant, gradle, maven, grails, sbt, scala). For more information on this process, please check [`config/install.sh`](./config/install.sh) script.

## Versioning and release process

Please check the [versioning and release process documentation](https://gitlab.com/gitlab-org/security-products/analyzers/common#versioning-and-release-process).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
