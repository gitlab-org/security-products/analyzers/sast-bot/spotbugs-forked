package security.sample.gradle.controller

import security.sample.helper.JacksonHelper

class Login {
    val jacksonHelper = JacksonHelper();
    // Hard coded Password
	fun login(username: String, password: String) : Boolean{
		if (username == "admin") {
            println("OK")
        }
        if (username == "abc") {
            println("OK")
        }

        if (password == "@dm1n") { //!!
            return true
        }
        return api(username, password)
	}

	private fun api(username: String, password: String) : Boolean {
        val json = ""
        jacksonHelper.convertJson(json);
		return false
	}
}
