package utils

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v5/writer"
)

const (
	// FlagJavaPath is the name of spotbug's cli java path argument
	FlagJavaPath = "javaPath"
	// FlagJavaVersion is the name of spotbug's cli java version argument
	FlagJavaVersion = "javaVersion"

	// JavaVersion17 represents java version -- 17
	JavaVersion17 = "17"
	// JavaVersion21 represents java version -- 21
	JavaVersion21 = "21"
	// DefaultJavaVersion is default version of Java chosen for project build
	DefaultJavaVersion = JavaVersion21

	// DocSASTPage is the doc page for Secure: SAST
	DocSASTPage = "https://docs.gitlab.com/ee/user/application_security/sast"
)

// Setup involves configuration of a environment for installation prerequisites
type Setup struct {
	cmd *exec.Cmd
}

// Run executes the Setup command, logs output, and returns potential errors
func (setup *Setup) Run() error {
	if setup.cmd == nil {
		return nil
	}

	streamingInfoLogger := log.StandardLogger().Writer()
	defer streamingInfoLogger.Close()

	setup.cmd.Stderr = streamingInfoLogger
	setup.cmd.Stdout = streamingInfoLogger

	if err := setup.cmd.Run(); err != nil {
		log.Errorf("Failed to set system Java: %s\n", err.Error())

		return err
	}
	return nil
}

// RunCmdError are errors for cmd runs that include an exit code
type RunCmdError struct {
	err      string
	exitCode int
}

// NewRunCmdError returns a new run command error
func NewRunCmdError(exitCode int, message string) error {
	return &RunCmdError{
		err:      message,
		exitCode: exitCode,
	}
}

func (e *RunCmdError) Error() string {
	return e.err
}

// RunCmd runs a command and gets the exit code.
func RunCmd(cmd *exec.Cmd) error {
	streamingInfoLogger := log.StandardLogger().Writer()
	defer streamingInfoLogger.Close()
	cmd.Stderr = streamingInfoLogger
	cmd.Stdout = streamingInfoLogger

	if err := cmd.Run(); err != nil {
		log.Errorf("command exec failure \n command: '%s'\n error: %v",
			cmd.String(), err)
		switch err.(type) {
		case *exec.ExitError:
			// The command failed during its execution
			exitError := err.(*exec.ExitError)
			waitStatus := exitError.Sys().(syscall.WaitStatus)
			return NewRunCmdError(waitStatus.ExitStatus(), err.Error())
		default:
			// The command couldn't even be executed
			return NewRunCmdError(1, fmt.Sprintf("Command couldn't be executed: %v", err))
		}
	}

	waitStatus := cmd.ProcessState.Sys().(syscall.WaitStatus)
	exitStatus := waitStatus.ExitStatus()

	if exitStatus != 0 {
		return NewRunCmdError(exitStatus, "Command returned a non zero exit status")
	}

	return nil
}

// RunCmdWithTextErrorDetection runs a command and returns an error code according to the presence
// of a string in the output.
// Uses code from https://github.com/kjk/go-cookbook in the public domain
func RunCmdWithTextErrorDetection(cmd *exec.Cmd, errorText string, message string) error {
	w := writer.NewBufferedWriter()
	defer func() {
		if err := w.Close(); err != nil {
			log.Warnf("Failed to close writer: %v", err)
		}
	}()
	cmd.Stderr = w
	cmd.Stdout = w

	if err := cmd.Run(); err != nil {
		log.Errorf("command exec failure \n command: '%s'\n error: %v",
			cmd.String(), err)
		return err
	}

	// Detect error string
	if strings.Contains(w.BufferedOutput(), errorText) {
		// Error text is present, return error.
		return errors.New(message)
	}

	return nil
}

// SetupCmdNoStd sets up a command's directory and environment for execution.
func SetupCmdNoStd(projectPath string, cmd *exec.Cmd) *exec.Cmd {
	cmd.Dir = projectPath
	cmd.Env = os.Environ()
	return cmd
}

// WithWarning runs the function passed as argument and prints a warning if it returns an error
func WithWarning(warning string, fun func() error) {
	err := fun()
	if err != nil {
		log.Warnf("%s (%s)\n", warning, err.Error())
	}
}

// SetupSystemJava sets up the system so that SpotBugs and it's dependencies (e.g. Maven) use the same Java
func SetupSystemJava(c *cli.Context) *Setup {
	if usesCustomJavaPath(c) {
		return &Setup{}
	}

	javaVersion := selectedSystemJava(c)

	return &Setup{
		cmd: exec.Command(
			"/bin/bash",
			"-c",
			fmt.Sprintf("source /home/gitlab/.bashrc"+
				" && switch_to java %s", javaVersion)),
	}
}

// determine the version of the selected Java to use
func selectedSystemJava(c *cli.Context) string {
	logMsg := "Unsupported Java version set in SAST_JAVA_VERSION variable."

	switch inputVersion := strings.TrimSpace(c.String(FlagJavaVersion)); inputVersion {
	case JavaVersion21, JavaVersion17:
		log.Infof("SAST_JAVA_VERSION was configured, using Java version %q.", inputVersion)
		return inputVersion
	case "":
		logMsg = "Java version not set in SAST_JAVA_VERSION variable."
	}

	log.Warnf("%s Using Java version %q as a fallback."+
		"\nRead more about setting Java version under Analyzer Settings at: %s", logMsg, DefaultJavaVersion, DocSASTPage)
	return DefaultJavaVersion
}

// JavaPath determines the path to the java executable
func JavaPath(c *cli.Context) string {
	if usesCustomJavaPath(c) {
		return c.String(FlagJavaPath)
	}

	return "java"
}

// returns based on whether or not using a custom Java
func usesCustomJavaPath(c *cli.Context) bool {
	return c.String(FlagJavaPath) != "java" && c.String(FlagJavaPath) != ""
}
