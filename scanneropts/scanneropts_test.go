package scanneropts

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestScannerOpts(t *testing.T) {
	tcs := []struct {
		usecase string
		input   string
		want    []string
	}{
		{
			usecase: "valid format with allowed values",
			input:   "-effort min",
			want:    []string{"-effort:min"},
		},
		{
			usecase: "valid format with allowed values - variant 2",
			input:   "-effort=min",
			want:    []string{"-effort:min"},
		},
		{
			usecase: "valid format with a mix of allowed and not allowed values",
			input:   "-exclude test.xml -effort less",
			want:    []string{"-effort:less"},
		},
		{
			usecase: "valid format with not allowed values and mandatory included",
			input:   "-include test2.xml -exclude test.xml",
			want:    []string{"-effort:max"},
		},
	}

	for _, tc := range tcs {
		tc := tc
		t.Run(tc.usecase, func(t *testing.T) {
			got := ParseConfigurableOpts(tc.input)
			require.ElementsMatch(t, tc.want, got)
		})
	}

}
