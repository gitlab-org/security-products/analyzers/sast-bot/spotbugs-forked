package writer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBufferedOutput(t *testing.T) {
	t.Run("Stores streamed log data in a buffer", func(t *testing.T) {
		blw := NewBufferedWriter()

		logData := "Test data"
		_, err := blw.Write([]byte(logData))
		assert.NoError(t, err)

		output := blw.BufferedOutput()
		assert.Equal(t, logData, output)
	})
}
