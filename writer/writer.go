package writer

import (
	"bytes"
	"io"

	log "github.com/sirupsen/logrus"
)

// BufferedWriter is a wrapper around an io.WriteCloser that also writes to a buffer
type BufferedWriter struct {
	w      io.WriteCloser
	buffer *bytes.Buffer
}

// Write writes the data to the underlying writer and the buffer
func (tw *BufferedWriter) Write(p []byte) (int, error) {
	n, err := tw.w.Write(p)
	if err == nil {
		_, err = tw.buffer.Write(p)
	}
	return n, err
}

// Close closes the underlying writer
func (tw *BufferedWriter) Close() error {
	return tw.w.Close()
}

// BufferedOutput returns the contents of the buffer
func (tw *BufferedWriter) BufferedOutput() string {
	return tw.buffer.String()
}

// NewBufferedWriter returns a BufferedWriter, which logs output using the Writer() default `info` level
// It also stores the logs in an internal buffer
func NewBufferedWriter() *BufferedWriter {
	return &BufferedWriter{
		w:      log.StandardLogger().Writer(),
		buffer: &bytes.Buffer{},
	}
}
