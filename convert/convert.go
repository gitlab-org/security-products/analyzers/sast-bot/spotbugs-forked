// Package convert translates a SpotBugs XML report into a issue.Report.
package convert

import (
	"encoding/xml"
	"io"

	report "gitlab.com/gitlab-org/security-products/analyzers/report/v5"
	ruleset "gitlab.com/gitlab-org/security-products/analyzers/ruleset/v3"
	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v5/instance"
	"gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v5/metadata"
)

// ref: https://gitlab.com/gitlab-org/security-products/security-report-schemas/-/blob/v15.0.4/src/security-report-format.json?ref_type=tags#L375
const shortMessageMaxLen = 255

// Convert translate a SpotBugs XML report into a issue.Report.
func Convert(reader io.Reader, prependPath string, rulesetConfig *ruleset.Config) (*report.Report, error) {
	var doc = struct {
		BugInstances []instance.Instance `xml:"BugInstance"`
	}{}

	err := xml.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	vulns := make([]report.Vulnerability, len(doc.BugInstances))
	for i, bug := range doc.BugInstances {
		name := bug.ShortMessage
		if len(bug.ShortMessage) > shortMessageMaxLen {
			name = bug.ShortMessage[:shortMessageMaxLen]
		}

		vulns[i] = report.Vulnerability{
			Category:    metadata.Type,
			Scanner:     &metadata.IssueScanner,
			Name:        name,
			Description: bug.LongMessage, // Could be extracted from BugPattern/Details instead
			CompareKey:  bug.CompareKey(),
			Severity:    bug.Severity(),
			Confidence:  bug.Confidence(),
			// Solution: bug.Solution(), Need to parse BugPattern/Details to extract solution
			Location:    bug.Location(prependPath),
			Identifiers: bug.Identifiers(),
			// Links:    bug.Links(), Need to parse BugPattern/Details to extract links
		}
	}

	var newReport = report.NewReport()
	newReport.Analyzer = metadata.AnalyzerID
	newReport.Config.Path = ruleset.PathSAST
	newReport.Vulnerabilities = vulns
	newReport.FilterDisabledRules(rulesetConfig)
	return &newReport, nil
}
