ARG POST_ANALYZER_SCRIPTS_VERSION=0.2.0
ARG TRACKING_CALCULATOR_VERSION=2.4.0

FROM registry.gitlab.com/security-products/post-analyzers/scripts:${POST_ANALYZER_SCRIPTS_VERSION} AS scripts
FROM registry.gitlab.com/security-products/post-analyzers/tracking-calculator:${TRACKING_CALCULATOR_VERSION} AS tracking

FROM golang:1.22.3 AS build
ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .

# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

FROM alpine:3.14

ARG ANT_VERSION
ARG FINDSECBUGS_VERSION

ENV FINDSECBUGS_VERSION ${FINDSECBUGS_VERSION:-1.12.0}

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-4.8.6}

ENV ASDF_VERSION "v0.10.2"
ENV ASDF_DATA_DIR="/opt/asdf"
ENV ASDF_DEFAULT_TOOL_VERSIONS_FILENAME=".gl-tool-versions"

RUN apk add --update --no-cache bash

COPY spotbugs /spotbugs

# Install analyzer
COPY --from=build /go/src/app/analyzer /analyzer-binary
COPY --from=tracking /analyzer-tracking /analyzer-tracking
# analyzer-start runs from within this projects custom start.sh
COPY --from=scripts /start.sh /analyzer-start
RUN chmod +x /analyzer-start

ENTRYPOINT []
ADD start.sh /analyzer
RUN chmod +x /analyzer

RUN mkdir -p /etc/ssl/certs/ && \
    touch /etc/ssl/certs/ca-certificates.crt && \
    chmod g+w /etc/ssl/certs/ca-certificates.crt

RUN mkdir -p /home/gitlab /.cache /builds/app /app && \
    chmod -R g+rw /.cache /builds && \
    addgroup -g 1000 gitlab && \
    adduser -u 1000 -G gitlab -D gitlab # -D=Don't assign a password

COPY --chown=gitlab config /home/gitlab

WORKDIR /home/gitlab

RUN bash /home/gitlab/install.sh

ENV HOME=/home/gitlab
ENV MAVEN_REPO_PATH=/home/gitlab

CMD ["/analyzer", "run"]
